package Vending_Machine;

import java.util.Scanner;

public class vendingMachine2 {
	
	private float p,n,v;
	DataStore2 DS2=new DataStore2();
	vendingMachineMDA vmMDA= new vendingMachineMDA(); 
	
	
	
	/*private void CREATE(float p){
		this. p = p;
	}
	
	private void COIN(float v){
		this.v = v;
}
	
	
	private void COFFEE(){
		vendingMachineMDA v = new vendingMachineMDA();
		v.Drink(1);
	}
	
	private void CREAM(){
		vendingMachineMDA v = new vendingMachineMDA();
		v.Drink(2);
	}
	
	
	private void InsertCups(int n){
		vendingMachineMDA v = new vendingMachineMDA();
		v.insertCup();
	}
	
	 
	
	
	private void SUGAR(){
		vendingMachineMDA v = new vendingMachineMDA();
		v.additive(1);
		
	}
	
	private void SetPrice(float p){
		vendingMachineMDA v = new vendingMachineMDA();
		v.setPrice();// TODO 
	}

	private void CANCEL(){
		vendingMachineMDA v = new vendingMachineMDA();
		v.cancel();
	}
*/
	
	
	public void CREATE(float p){
		DS2.StorePrice(p);
		vmMDA.create();
}

public void COIN(float v){
	DS2.storeValue(v);
	
	if((DS2.returnCoins()+v) >= DS2.getPrice())
	vmMDA.coin(1);
	
	else
		vmMDA.coin(0);
}

public void SUGAR(){
	DS2.SetAdditive(2);//set additive to int in MDA
	vmMDA.additive();
}

public void CREAM(){
	DS2.SetAdditive(1);//set additive to int in MDA
	vmMDA.additive();
}

public void COFFEE(){
	DS2.SetDrinkID(1);
	vmMDA.dispose_drink(1);
}



public void InsertCups(int n){
	vmMDA.insertcups(n);
}

public void SetPrice(float p){
	DS2.StorePrice(p);
	vmMDA.set_price();
}

public void CANCEL(){
	
	vmMDA.cancel();
}
	
	
	
	public void display(){
		int  no_of_cups;
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Vending Machine 2 selected\n");
		System.out.println("MENU of Operations\n" +
				" 0. CREATE (float) " +
				" 1. COIN (float) "  +
				" 2. SUGAR ()" +
				" 3. COFFEE ()" +
				" 4. cream()" +
				" 5. InsertCups(int)" +
				" 6. SetPrice(int)" +
				" 7. CANCEL()" +
				" 8. Quit the application"	);
		
		DS2.setVM(1);
	
	System.out.println("Vending Machine-1 Execution");
	int ch = 1;
	float p,v,price;
	while (ch != 8)
		{
		System.out.println("Select Operation: \n" +
		"0-CREATE,1-COIN,2-SUGAR,3-COFFEE,4-CREAM,5-Insert Cups,6-Set Price,7-Cancel");
		ch =reader.nextInt();
		 switch (ch) { 
		    case 0:   //create
				System.out.println("  Operation:  CREATE (float p)");
				System.out.println("  Enter value of the parameter p:");
				p = reader.nextFloat();
				CREATE(p);
				break;

		   case 1:  //coin
				System.out.println("  Operation:  COIN(float v)");
				System.out.println("  Enter value of the parameter v:");
				v = reader.nextFloat();
				COIN(v);
				break;

		    case 2:  //sugar
				System.out.println("  Operation:  SUGAR()");
				SUGAR();
				break;
					
		    case 3:  // tea
				System.out.println("  Operation:  COFFEE()");
				COFFEE();
				break;

		    case 4:  // chocolate
				System.out.println("  Operation:  CREAM()");
				CREAM();
				break;

		    case 5:  // insert_cups
				System.out.println("  Operation:  InsertCups(int n)");
				System.out.println("  Enter value of the parameter n:");
				no_of_cups = reader.nextInt();
				InsertCups(no_of_cups);
				break;

		    case 6:  // set_price
				System.out.println("  Operation:  set_price(int p)");		
				System.out.println("  Enter value of the parameter p:");
				price = reader.nextFloat();
				SetPrice(price);
				break;

		    case 7:  // cancel
				System.out.println("  Operation:  CANCEL()");
				CANCEL(); 
				break;
				
	      }
	 		
		}

	
	
	}
	
}
