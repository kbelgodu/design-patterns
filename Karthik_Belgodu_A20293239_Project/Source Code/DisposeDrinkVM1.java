package Vending_Machine;

public class DisposeDrinkVM1 extends DisposeDrink{

	private DataStore1 DS1;

    public DisposeDrinkVM1(DataStore1 DS1) {
        this.DS1 = DS1;
    }

    @Override
    public void DisposeDrink() {
    	int cf=DS1.getDrinkID();
    	if(cf==1)
    	{
    	System.out.println("Tea Disposed");
    	outPutProcessor op =new outPutProcessor(); 
    	op.ZeroCF();
    	//op.ReturnCoins();
    	}
    	else if(cf==2)
    	{
        	System.out.println("Chocolate Disposed");
        	outPutProcessor op =new outPutProcessor(); 
        	op.ZeroCF();
    	}	
    }
	
}
