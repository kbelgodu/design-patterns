package Vending_Machine;

public class IncCFVM1 extends IncCF{
		
	private DataStore1 DS1;

    public IncCFVM1(DataStore1 DS1) {
        this.DS1 = DS1;
    }

    @Override
    public void IncreaseCF() {
    	int cf=DS1.getValue();
    	System.out.println("Cumulative Fund set to : "+cf);
    	DS1.insertedValue(cf);
    }
}
