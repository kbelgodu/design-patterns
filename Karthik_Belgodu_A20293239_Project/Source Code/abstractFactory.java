package Vending_Machine;

public abstract class abstractFactory {
	
	public 	abstract DataStore createDatastore();
	
	public  outPutProcessor createoutPutProcessor(DataStore data){
		final outPutProcessor op = new outPutProcessor();
		op.StorePrice();
		op.ZeroCF();
		op.IncCF();
		op.ReturnCoins();
		op.DisposeDrink();
		op.DisposeAdditive();
		return op;
	}

}
