package Vending_Machine;

public class StorePriceVM1 extends StorePrice{
	
	private DataStore1 DS1;

    public StorePriceVM1(DataStore1 DS1) {
        this.DS1 = DS1;
    }

    @Override
    public void StorePrice() {
    	int pr=DS1.getPrice();
    	System.out.println("Price Stored is :"+pr);
    	DS1.StorePrice(pr);
    }

}
