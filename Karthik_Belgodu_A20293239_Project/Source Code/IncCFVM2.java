package Vending_Machine;

public class IncCFVM2 extends IncCF{
		
	private DataStore2 DS2;

    public IncCFVM2(DataStore2 DS2) {
        this.DS2 = DS2;
    }

    @Override
    public void IncreaseCF() {
    	float cf=DS2.getValue();
    	System.out.println("Cumulative Fund set to : "+cf);
    	DS2.insertedValue(cf);
    }
	
}
