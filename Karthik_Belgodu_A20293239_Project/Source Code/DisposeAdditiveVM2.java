package Vending_Machine;

public class DisposeAdditiveVM2 extends DisposeAdditive{
	
	private DataStore2 DS2;

    public DisposeAdditiveVM2(DataStore2 DS2) {
        this.DS2 = DS2;
    }

    @Override
    public void DisposeAdditive() {
    	String[] cf=DS2.GetAdditive();
    	//System.out.println();
    	
    	if(cf[0]=="cream")
    	System.out.println("Cream Disposed");
    	
    	else if(cf[1]=="sugar")
        	System.out.println("sugar Disposed");
    	
    	else 
        	System.out.println("Sugar and Cream Disposed");
    }

}
