package Vending_Machine;

public abstract class DataStore {
	
	
	
	public abstract void clearValue();
	

	
	public abstract void SetAdditive(int add);
	
	 public abstract String[] GetAdditive();
 
 
	 public abstract void SetDrinkID(int d);
		
		public abstract int getDrinkID();
	 
	public abstract int updateCups(int p);
	
	public abstract void insertCups(int p);
	
	public abstract void setVM(int p);
}

