package Vending_Machine;

public class Inserted extends StateClass {
	
	outPutProcessor op= new outPutProcessor(); 
	
	public void dispose_drink(){
		
		op.DisposeDrink();
	}
	
	public void cancel(){
		op.ReturnCoins();
		op.ZeroCF();
		
	}
	
	public void additive(){
		
		op.DisposeAdditive();
		
	}
	
	public void coin(){
		//call return coins in op
		
		op.ReturnCoins();
	}
	
}
