package Vending_Machine;

import java.util.Scanner;

public class vendingMachine1 {

	private int p,n,v;
	DataStore1 DS1=new DataStore1();
	vendingMachineMDA vmMDA= new vendingMachineMDA(); 
	
		
	public void create(int p){
			DS1.StorePrice(p);
			
			vmMDA.create();
	}
	
	public void coin(int v){
		DS1.storeValue(v);
		
		if((DS1.returnCoins()+v) >= DS1.getPrice())
		vmMDA.coin(1);
		
		else
			vmMDA.coin(0);
}
	
	public void sugar(){
		DS1.SetAdditive(1);
		vmMDA.additive();
}
	
	public void tea(){
		DS1.SetDrinkID(1);
		vmMDA.dispose_drink(1);
}
	
	public void chocolate(){
		DS1.SetDrinkID(2);
		vmMDA.dispose_drink(2);
}
	
	public void insert_cups(int n){
		vmMDA.insertcups(n);//todo insert cups in mda
		DS1.insertCups(n);
}
	
	public void set_price(int p){
		DS1.StorePrice(p);
		vmMDA.set_price();
}
	
	public void cancel(){
		
		vmMDA.cancel();
}
	
	
	public void display(){
		int p, v, no_of_cups, price;
		Scanner reader = new Scanner(System.in);
		System.out.println("Vending Machine 1 selected\n");
		System.out.println("MENU of Operations\n" +
				" 0. create(int) " +
				" 1. coin(int) "  +
				" 2. sugar()" +
				" 3. tea()" +
				" 4. chocolate()" +
				" 5. insert_cups(int)" +
				" 6. set_price(int)" +
				" 7. cancel()" +
				" 8. Quit the application"	);
		
		DS1.setVM(1);
	
	System.out.println("Vending Machine-1 Execution");
	int ch = 1;
	
	while (ch != 8)
		{
		System.out.println("Select Operation: \n" +
		"0-create,1-coin,2-sugar,3-tea,4-chocolate,5-insert_cups,6-set_price,7-cancel");
		ch =reader.nextInt();
		 switch (ch) { 
		    case 0:   //create
				System.out.println("  Operation:  create(int p)");
				System.out.println("  Enter value of the parameter p:");
				p = reader.nextInt();
				create(p);
				break;

		   case 1:  //coin
				System.out.println("  Operation:  coin(int v)");
				System.out.println("  Enter value of the parameter v:");
				v = reader.nextInt();
				coin(v);
				break;

		    case 2:  //sugar
				System.out.println("  Operation:  sugar()");
				sugar();
				break;
					
		    case 3:  // tea
				System.out.println("  Operation:  tea()");
				tea();
				break;

		    case 4:  // chocolate
				System.out.println("  Operation:  chocolate()");
				chocolate();
				break;

		    case 5:  // insert_cups
				System.out.println("  Operation:  insert_cups(int n)");
				System.out.println("  Enter value of the parameter n:");
				n = reader.nextInt();
				insert_cups(n);
				break;

		    case 6:  // set_price
				System.out.println("  Operation:  set_price(int p)");		
				System.out.println("  Enter value of the parameter p:");
				price = reader.nextInt();
				set_price(price);
				break;

		    case 7:  // cancel
				System.out.println("  Operation:  cancel()");
				cancel();
				break;
				
	      }
		}
	}
}
