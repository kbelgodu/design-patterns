package Vending_Machine;

public class ReturnCoinsVM1 extends ReturnCoins{
	private DataStore1 DS1;

    public ReturnCoinsVM1(DataStore1 DS1) {
        this.DS1 = DS1;
    }

    @Override
    public void ReturnCoins() {
    	int cf=DS1.getV();
    	System.out.println(cf+" Coins returned ");
    	DS1.clearV();
    }
}
