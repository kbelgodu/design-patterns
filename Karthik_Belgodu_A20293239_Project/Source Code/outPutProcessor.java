package Vending_Machine;

public class outPutProcessor {
	
	DataStore1 DS1= new DataStore1();
	DataStore2 DS2= new DataStore2();
	private int vm1,vm2;
	
	public void StorePrice(){
		
		if(DS1.getVM()==1)
		{
			vm1=1;
			StorePriceVM1 sp= new StorePriceVM1(DS1);
			sp.StorePrice();
		}	
		else if(DS2.getVM()==1)
		{
			vm2=1;
			StorePriceVM2 sp= new StorePriceVM2(DS2);
			sp.StorePrice();
		}	
	}
	
	public void ZeroCF(){
		
		if(DS1.getVM()==1)
		{
			ZeroCFVM1 ZF=new ZeroCFVM1();
			ZF.ZeroCF();
		}
		else
		{
			ZeroCFVM2 ZF=new ZeroCFVM2();
			ZF.ZeroCF();
		}
		
	}
	
	public void IncCF(){
		
		if(DS1.getVM()==1)
		{
			IncCFVM1 in= new IncCFVM1(DS1);
			in.IncreaseCF();
		}
		else
		{
			IncCFVM2 in = new IncCFVM2(DS2);
			in.IncreaseCF();
		}
		
	}
	public void ReturnCoins(){
		
		if(DS1.getVM()==1)
		{
			ReturnCoinsVM1 rc = new ReturnCoinsVM1(DS1);
			System.out.println("hi");
			rc.ReturnCoins();
		}
		else
		{
			ReturnCoinsVM2 rc = new ReturnCoinsVM2(DS2);
			
			rc.ReturnCoins();
		}
	
	}
	public void DisposeDrink(){
		
		if(DS1.getVM()==1)
		{
			DisposeDrinkVM1 dd= new DisposeDrinkVM1(DS1);
			
			dd.DisposeDrink();
		}
		else
		{
			DisposeDrinkVM2 dd= new DisposeDrinkVM2(DS2);
			
			dd.DisposeDrink();
		}
	
	}
	public void DisposeAdditive(){
		
		if(DS1.getVM()==1)
		{
			DisposeAdditiveVM1 da= new DisposeAdditiveVM1(DS1);
			da.DisposeAdditive();
		}
		else
		{
			DisposeAdditiveVM2 da= new DisposeAdditiveVM2(DS2);
			da.DisposeAdditive();
		}
	
	}
	
	//insert cup{}
}
