package Vending_Machine;



	public interface Interface1 {
		   // Funtions Implemented by Vending Machine 1 
		    public int create(int price);
		    public int insertcups(int n);
		    public int coin(int v);
		    public void cancel();
		    public int setprice(int p);
		    public void sugar();
		    public void tea();
		    public void chocolate();
		    
		    // Functions Implemented by Vending Machine 2
		    public float CREATE(float price);
		    public float COIN(float v);
		    public void CANCEL();
		    public void SUGAR();
		    public void CREAM();
		    public void COFFEE();
		    public int Insertcups(int n);
		    public float Setprice(float p);
		    
		    
		 }
	
	

