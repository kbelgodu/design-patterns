package Vending_Machine;

public class ReturnCoinsVM2 extends ReturnCoins{
	private DataStore2 DS2;

    public ReturnCoinsVM2(DataStore2 DS2) {
        this.DS2 = DS2;
    }

    @Override
    public void ReturnCoins() {
    	float cf=DS2.getV();
    	System.out.println(cf+" Coins returned");
    	DS2.clearV();
    }
}
