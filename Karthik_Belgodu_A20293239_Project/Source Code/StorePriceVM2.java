package Vending_Machine;

public class StorePriceVM2 extends StorePrice{
	
	private DataStore2 DS2;

    public StorePriceVM2(DataStore2 DS2) {
        this.DS2 = DS2;
    }

    @Override
    public void StorePrice() {
    	float pr=DS2.getPrice();
    	System.out.println("Price Stored is :"+pr);
    	DS2.StorePrice(pr);
    }
	


}
