package Vending_Machine;

public class DisposeAdditiveVM1 extends DisposeAdditive{
	
	private DataStore1 DS1;

    public DisposeAdditiveVM1(DataStore1 DS1) {
        this.DS1 = DS1;
    }

    @Override
    public void DisposeAdditive() {
    	String[] cf=DS1.GetAdditive();
    	
    	if(cf[0]=="sugar")
    	System.out.println("Sugar Disposed");
    }

}
